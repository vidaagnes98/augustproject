CREATE TABLE IF NOT EXISTS performance (
  id VARCHAR(36) PRIMARY KEY,
  ticketType VARCHAR(64) NOT NULL,
  status VARCHAR(64) NOT NULL,
  transactionDate DATE NOT NULL,
  name VARCHAR(64) NOT NULL
);

CREATE INDEX IF NOT EXISTS performance_transactionDate ON performance (transactionDate);
CREATE INDEX IF NOT EXISTS performance_name ON performance (name);