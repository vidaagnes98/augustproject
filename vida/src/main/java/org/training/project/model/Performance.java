package org.training.project.model;

import java.util.Date;

public class Performance {
	
	private String id;
    private String ticketType;
    private String status;
    private Date transactionDate;
    private String name;
    
    public Performance () {
    	
    }
    
    public Performance (String id, String ticketType, String status, Date transactionDate, String name) {
        this.id = id;
        this.ticketType = ticketType;
        this.status = status;
        this.transactionDate = transactionDate;
        this.name =name;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String type) {
		this.ticketType = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
    public String toString() {
        return "Performance [id=" + id + ", type=" + ticketType + ", status=" + status + ", transactionDate="
                + transactionDate + ", name=" + name + "]";
    }

    
}
