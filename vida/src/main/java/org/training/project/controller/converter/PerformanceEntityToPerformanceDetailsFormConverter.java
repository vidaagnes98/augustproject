package org.training.project.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.training.project.controller.form.PerformanceDetailsForm;
import org.training.project.model.Performance;

@Component
public class PerformanceEntityToPerformanceDetailsFormConverter implements Converter<Performance, PerformanceDetailsForm> {

	@Override
    public PerformanceDetailsForm convert(Performance source) {

        PerformanceDetailsForm p = new PerformanceDetailsForm();

        p.setId(source.getId());
        p.setTicketType(source.getTicketType());
        p.setStatus(source.getStatus());
        p.setTransactionDate(source.getTransactionDate());
        p.setName(source.getName());

        return p;
    }
	
}
