package org.training.project.controller.form;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class PerformanceDetailsForm {
	
	@NotNull
    private String id;

    @NotEmpty
    @Size(max = 64)
    private String ticketType;
    
    @NotEmpty
    @Size(max = 64)
    private String status;

    @NotNull
    private Date transactionDate;

    @NotEmpty
    @Size(max = 64)
    private String name;
    
    @NotNull
    private Action action;

    public static enum Action {
        SAVE, BACK, DELETE
    }
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

    @Override
    public String toString() {
        return "PerformanceDetailsForm [id=" + id + ", ticketType=" + ticketType + ", status=" + status
                + ", transactionDate=" + transactionDate + ", name=" + name + ", action=" + action + "]";
    }




}
