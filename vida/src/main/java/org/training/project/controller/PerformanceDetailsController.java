package org.training.project.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.training.project.model.Performance;
import org.training.project.repository.PerformanceRepository;
import org.training.project.controller.form.PerformanceDetailsForm;


@Controller
@RequestMapping("/performance-details.html")
public class PerformanceDetailsController {

	private static final String VIEW_PERFORMANCE_DETAILS = "performance-details";
	
	@Autowired
    private PerformanceRepository performanceRepository;
	
	@Resource
    private ConversionService conversionService;
	
	@RequestMapping(method = RequestMethod.GET)
    public String displayPerformance(Model model, @RequestParam(required = false) String id) {

        Performance performance = id != null ? performanceRepository.getById(id) : new Performance();

        PerformanceDetailsForm form = conversionService.convert(performance,
                PerformanceDetailsForm.class);

        model.addAttribute("performanceForm", form);
        return VIEW_PERFORMANCE_DETAILS;
    }
	
	@RequestMapping(method = RequestMethod.POST)
    public String createOrUpdatePerformance(@ModelAttribute("performanceForm") @Valid PerformanceDetailsForm performanceForm,
            BindingResult bindingResult) {

        switch (performanceForm.getAction()) {

        case SAVE:

            if (bindingResult.hasErrors()) { // if a binding error happened send back the user the form for correction
                bindingResult.reject("performanceDetails.error.userInput");
                return VIEW_PERFORMANCE_DETAILS;
            }

            Performance performance = conversionService.convert(performanceForm, Performance.class);

            if (performanceForm.getId() == null) {
            	performanceRepository.create(performance);
            } else {
            	performanceRepository.update(performance);
            }
            break;

        case DELETE:
        	performanceRepository.deleteById(performanceForm.getId());
            break;
        }

        return "redirect:/";

    }
	
	@ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleNotExistingPerformance(EmptyResultDataAccessException e) {
        return "error/performance-does-not-exist";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true /* empty as null */));
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true /* allow empty */));
    }
	
}
