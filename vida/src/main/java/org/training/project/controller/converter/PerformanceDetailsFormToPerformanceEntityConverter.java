package org.training.project.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.training.project.controller.form.PerformanceDetailsForm;
import org.training.project.model.Performance;

@Component
public class PerformanceDetailsFormToPerformanceEntityConverter implements Converter<PerformanceDetailsForm, Performance> {
	
	@Override
    public Performance convert(PerformanceDetailsForm source) {
        Performance p = new Performance();
        p.setId(source.getId());
        p.setTicketType(source.getTicketType());
        p.setStatus(source.getStatus());
        p.setTransactionDate(source.getTransactionDate());
        p.setName(source.getName());
        
        return p;
    }

}
