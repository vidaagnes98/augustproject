package org.training.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.training.project.service.PerformanceService;

@Controller
public class PerformanceListController {
	
	@Autowired
	private PerformanceService performanceService;
	
	@RequestMapping(value = "/")
    public String displayPerformanceList(Model model, @RequestParam(required = false) String searchTerm) {

        model.addAttribute("performances", performanceService.searchByName(searchTerm));
        return "performance-list";
    }

}
