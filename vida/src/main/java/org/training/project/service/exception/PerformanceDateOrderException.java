package org.training.project.service.exception;

public class PerformanceDateOrderException extends Exception {
	
	public PerformanceDateOrderException(String s){
		super (s);
	}

}
