package org.training.project.service.exception;

public class ResourceUnavailableException extends PerformanceException {
	
	public ResourceUnavailableException(String s) {
        super(s);
    }

}
