package org.training.project.service;

import java.util.List;

import org.training.project.model.Performance;

public interface PerformanceService {
	
	List<Performance> searchByName(String searchTerm);

}
