package org.training.project.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.training.project.model.Performance;
import org.training.project.repository.PerformanceRepository;
import org.training.project.service.PerformanceService;

@Service
public class PerformanceServicelmpl implements PerformanceService {
	
	@Autowired
    private PerformanceRepository performanceRepository;
	
	public List<Performance> searchByName(String searchTerm) {

        List<Performance> performanceResult = null;

        if (searchTerm == null) {
        	performanceResult = performanceRepository.findAll();
        } else {
            searchTerm = searchTerm.trim();
            performanceResult = searchTerm.length() == 0 ? performanceRepository.findAll()
                    : performanceRepository.findByTypeOrStatusOrNameIgnoreCaseContaining(searchTerm);
        }

        return performanceResult;
    }
	
	/*@Transactional
    public void replaceTicketStatus(String newDomain, String... performanceIds) {

        Assert.notNull(newDomain);
        Assert.hasText(newDomain);
        Assert.notEmpty(performanceIds);

        newDomain = "@" + newDomain;
        boolean stop = false;

        for (String id : performanceIds) {
            Performance e = performanceRepository.getById(id);
            String status = e.getStatus();
            status = status.replaceAll("@.+", newDomain);

            e.setStatus(status);
            performanceRepository.update(e);

        }
    }*/
		
}
