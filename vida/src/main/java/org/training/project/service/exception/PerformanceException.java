package org.training.project.service.exception;

public class PerformanceException extends Exception {
	
	public PerformanceException (String s) {
		super(s);
	}

}