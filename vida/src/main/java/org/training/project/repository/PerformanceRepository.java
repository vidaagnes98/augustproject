package org.training.project.repository;

import java.time.LocalDate;
import java.util.List;
import org.training.project.model.Performance;

public interface PerformanceRepository extends CRUDRepository<Performance, String> {

    List<Performance> getPerformanceAfterThisTransactionDate(LocalDate earliestTransactionDate);

    List<Performance> findByTypeOrStatusOrNameIgnoreCaseContaining(String s);
}
