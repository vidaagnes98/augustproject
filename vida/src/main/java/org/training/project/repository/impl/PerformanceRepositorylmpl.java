package org.training.project.repository.impl;

import static org.springframework.util.Assert.notNull;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.training.project.model.Performance;
import org.training.project.repository.PerformanceRepository;

@Repository
public class PerformanceRepositorylmpl implements PerformanceRepository {
	
	 @Autowired
	    private JdbcTemplate jdbcTemplate;

	    private RowMapper<Performance> rowMapper = new RowMapper<Performance>() {
	        public Performance mapRow(ResultSet rs, int rowNum) throws SQLException {
	            Performance p = new Performance();
	            p.setId(rs.getString("id"));
	            p.setTicketType(rs.getString("ticketType"));
	            p.setStatus(rs.getString("status"));
	            p.setTransactionDate(rs.getDate("transactionDate"));
	            p.setName(rs.getString("name"));
	            return p;
	        }
	    };
	    
	    public List<Performance> getPerformanceAfterThisTransactionDate (LocalDate earliestTransactionDate) {

	        notNull(earliestTransactionDate, "The earliestTransactionDate can not be null");

	        return jdbcTemplate.query(
	                "SELECT id, ticketType, status, transactiondate, name FROM Performance where transactionDate >= ? ORDER BY transactionDate",
	                new Object[] { Date.valueOf(earliestTransactionDate) }, rowMapper);

	    }
	    
	    public List<Performance> findAll() {
	        return jdbcTemplate.query("SELECT id, ticketType, status, transactiondate, name FROM Performance ORDER BY name",
	                new Object[] {}, rowMapper);
	    }
	    
	    @Override
	    public void create(Performance p) {

	        notNull(p, "The entity can not be null");

	        if (p.getId() == null) {
	            p.setId(UUID.randomUUID().toString());
	        }
	        jdbcTemplate.update("INSERT INTO employee (id, ticketType, status, transactiondate, name) VALUES (?, ?, ?, ?, ?)", p.getId(),
	                p.getTicketType(), p.getStatus(), p.getTransactionDate(), p.getName());
	    }
	    
	    @Override
	    public Performance getById(String id) { // may throw EmptyResultDataAccessException or
	                                         // IncorrectResultSizeDataAccessException
	        notNull(id, "The id can not be null");

	        Performance p = jdbcTemplate.queryForObject("SELECT id, ticketType, status, transactiondate, name FROM Performance where id = ?",
	                new Object[] { id }, rowMapper);

	        if (p == null) {
	            throw new EmptyResultDataAccessException(1);
	        }
	        return p;
	    }
	    
	    @Override
	    public void update(Performance p) {
	        notNull(p, "The entity can not be null");
	        jdbcTemplate.update("UPDATE performance set ticketType = ?,  status = ?, transactiondate = ?, name = ? where id = ?", p.getTicketType(),
	                p.getStatus(), p.getTransactionDate(), p.getName(), p.getId());
	    }
	    
	    @Override
	    public void deleteById(String id) {
	        notNull(id, "The id can not be null");
	        jdbcTemplate.update("DELETE FROM performance where id = ?", id);
	    }
	    
	    @Override
	    public List<Performance> findByTypeOrStatusOrNameIgnoreCaseContaining(String s) {
	        notNull(s, "Search string can not be null");
	        String pattern = ("%" + s + "%").toLowerCase();
	        return jdbcTemplate.query(
	                "SELECT id, ticketType, status, transactiondate, name FROM Performance"
	                        + " WHERE LOWER(ticketType) LIKE ? OR LOWER(status) LIKE ? OR LOWER(name) LIKE ?"
	                        + " ORDER BY name",
	                new Object[] { pattern, pattern }, rowMapper);
	    }

}
